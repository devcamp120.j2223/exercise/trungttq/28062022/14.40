import { Component } from "react";

class Display2 extends Component{
    componentWillMount() {
        console.log('Component Will Mount');
    }

    componentDidMount() {
        console.log('Component Did Mount');
        
    }
    componentWillUnmount() {
        console.log("Component will unmount");
    }
    render(){
        return(
            <div>
                <button className="btn btn-danger" onClick={this.props.display}>Destroy Component</button>
                <p>I exist!</p>
            </div>
        )
    }
}
export default Display2;